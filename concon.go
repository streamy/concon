/*
Concon : Cancel blocking network Read and Write operations using context.Context
*/
package concon

import (
	"context"
	"net"
	"sync"
	"time"
)

// Conn is a stream oriented network connection with i/o operations that are controlled by Contexts
type Conn interface {
	ReadContext(ctx context.Context, b []byte) (n int, err error)
	WriteContext(ctx context.Context, b []byte) (n int, err error)
	Close() error
	LocalAddr() net.Addr
	RemoteAddr() net.Addr
}

// NewContextConn returns a Conn given a net.Conn.  The caller is not expected
// to use the *Timeout() methods on the underlying net.Conn after this.
func NewContextConn(nc net.Conn) Conn {
	return &ctxConn{c: nc}
}

type ctxConn struct {
	c   net.Conn
	rlk sync.RWMutex
	wlk sync.RWMutex
}

func (c *ctxConn) Close() error {
	return c.c.Close()
}

func (c *ctxConn) LocalAddr() net.Addr {
	return c.c.LocalAddr()
}

func (c *ctxConn) RemoteAddr() net.Addr {
	return c.c.RemoteAddr()
}

func (c *ctxConn) ReadContext(ctx context.Context, b []byte) (int, error) {
	return ioContext(&c.rlk, c.c.Read, c.c.SetReadDeadline, ctx, b)
}

func (c *ctxConn) WriteContext(ctx context.Context, b []byte) (int, error) {
	return ioContext(&c.wlk, c.c.Write, c.c.SetWriteDeadline, ctx, b)
}

func ioContext(lk *sync.RWMutex, ioFunc func([]byte) (int, error), dlFunc func(time.Time) error, ctx context.Context, b []byte) (int, error) {

	type ioResult struct {
		i   int
		err error
	}

	result := make(chan ioResult)

	go func() {
		for {
			lk.RLock()
			i, err := ioFunc(b)
			lk.RUnlock()
			ne, ok := err.(net.Error)
			switch {
			case ok && ne.Timeout():
				select {
				case <-ctx.Done():
					// we timed out because our context is done.
					result <- ioResult{0, ctx.Err()}
					return
				default:
					// we timed out because some other context is done. Re-enter the read.
				}
			default:
				result <- ioResult{i, err}
				return
			}
		}
	}()

	select {
	case <-ctx.Done():
		if err := dlFunc(inThePast); err != nil {
			panic("TODO: work out how best to handle this case")
		}
		r := <-result
		lk.Lock()
		// at this point, no i/o operation is running. Unset timeout and allow i/o once again.
		if err := dlFunc(noDeadline); err != nil {
			panic("TODO: work out how best to handle this case")
		}
		lk.Unlock()
		return r.i, r.err
	case r := <-result:
		return r.i, r.err
	}
}

var (
	inThePast  = time.Unix(1483516789, 0)
	noDeadline = time.Time{}
)
