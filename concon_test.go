package concon

import (
	"context"
	"fmt"
	"net"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestSimpleRead(t *testing.T) {
	assert := assert.New(t)

	a, b := tcpPipe()

	defer a.Close()
	defer b.Close()

	cc := NewContextConn(a)

	readErr := make(chan error, 1)
	go func() {
		buf := make([]byte, 1024)
		_, err := cc.ReadContext(context.Background(), buf)
		readErr <- err
	}()

	_, err := b.Write([]byte("foo bar baz"))
	assert.NoError(err)

	select {
	case err := <-readErr:
		assert.NoError(err)
	case <-time.After(200 * time.Millisecond):
		t.Error("read did not complete")
	}

}

func TestReadTimeout(t *testing.T) {
	assert := assert.New(t)

	a, b := tcpPipe()

	defer a.Close()
	defer b.Close()

	cc := NewContextConn(a)

	ctx, cancel := context.WithTimeout(context.Background(), 50*time.Millisecond)
	defer cancel()
	buf := make([]byte, 1024)

	_, err := cc.ReadContext(ctx, buf)

	assert.EqualError(err, "context deadline exceeded")
}

func TestTwoPendingReadsOneCancelManyTimes(t *testing.T) {

	assert := assert.New(t)

	a, b := tcpPipe()

	defer a.Close()
	defer b.Close()

	cc := NewContextConn(a)

	iterations := 1024

	for it := 0; it < iterations; it++ {

		count := 2

		cancels := make([]context.CancelFunc, count)
		cancelled := make(chan int, count)
		errors := make(chan error, count)

		for i := 0; i < count; i++ {
			ctx, cancel := context.WithCancel(context.Background())
			cancels[i] = cancel
			go func(i int, ctx context.Context) {
				buf := make([]byte, 1024)
				_, err := cc.ReadContext(ctx, buf)
				if err.Error() == "context canceled" {
					cancelled <- i
				} else {
					errors <- err
				}
			}(i, ctx)
		}

		for i := 0; i < count; i++ {
			cancels[i]()
			select {
			case e := <-errors:
				t.Error(e)
			case c := <-cancelled:
				assert.Equal(i, c)
			}

		}
	}
}

func TestMultiplePendingReadsOneCancel(t *testing.T) {

	assert := assert.New(t)

	a, b := tcpPipe()

	defer a.Close()
	defer b.Close()

	cc := NewContextConn(a)

	count := 128

	cancels := make([]context.CancelFunc, count)
	cancelled := make(chan int, count)
	errors := make(chan error, count)

	for i := 0; i < count; i++ {
		ctx, cancel := context.WithCancel(context.Background())
		cancels[i] = cancel
		go func(i int, ctx context.Context) {
			buf := make([]byte, 1024)
			_, err := cc.ReadContext(ctx, buf)
			if err.Error() == "context canceled" {
				cancelled <- i
			} else {
				errors <- err
			}
		}(i, ctx)
	}

	for i := 0; i < count; i++ {
		cancels[i]()
		select {
		case e := <-errors:
			t.Error(e)
		case c := <-cancelled:
			assert.Equal(i, c)
		}

	}

}

func tcpPipe() (net.Conn, net.Conn) {
	l, err := net.Listen("tcp", ":0")
	if err != nil {
		panic(err)
	}
	port := l.(*net.TCPListener).Addr().(*net.TCPAddr).Port
	svr := make(chan net.Conn)
	go func() {
		c, err := l.Accept()
		if err != nil {
			panic(err)
		}
		l.Close()
		svr <- c
	}()
	client, err := net.Dial("tcp", fmt.Sprintf("localhost:%d", port))
	if err != nil {
		panic(err)
	}
	return client, <-svr
}
