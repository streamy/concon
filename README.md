Concon
======

[![GoDoc](https://godoc.org/gitlab.com/streamy/concon?status.svg)](https://godoc.org/gitlab.com/streamy/concon)


Cancel blocking network Read and Write operations using context.Context


